﻿using Common.Model;
using Flickr.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Flickr.Helpers
{
    public interface IFlickrJSONHelper
    {
        Exif DeserializeExif(string json);
        Photo DeserializePhoto(string json);
        void AssignMissingPropertiesToPhoto(Photo img, Photo photo);
        DateTime ConvertRawToDateTime(string p);

        string GenerateReturnJSON(object param);
    }

    [Export(typeof(IFlickrJSONHelper))]
    public class FlickrJSONHelper : IFlickrJSONHelper
    {
        public string GenerateReturnJSON(object param)
        {
            return JsonConvert.SerializeObject(param, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
        }

        public DateTime ConvertRawToDateTime(string p)
        {
            var dates = p.Split(':', ' ');
            var year = int.Parse ( dates[1]);
            var month = int.Parse ( dates[2]);
            var day = int.Parse ( dates[3]);
            var hour = int.Parse ( dates[4]);
            var min = int.Parse ( dates[5]);
            var sec = int.Parse ( dates[6]);

            return new DateTime(year: year, month: month, day: day, hour: hour, minute: min, second: sec);
        }

        public Exif DeserializeExif(string json)
        {
            json = json.Replace("\"", "");
            return new Exif {
                Tagspace = TraverseString(json, "tagspace"),
                TagspaceID = TraverseString(json, "tagspaceid"),
                Tag = TraverseString(json, "tag"),
                Label = TraverseString(json, "label"),
                Raw = TraverseString(json, "_content", Environment.NewLine)
            };
        }

        public void AssignMissingPropertiesToPhoto(Photo img, Photo photo)
        {
            if (photo.Camera == null && img.Camera != null)
                photo.Camera = img.Camera;
            if (photo.ID == null && img.ID != null)
                photo.ID = img.ID;
            if (photo.Secret == null && img.Secret != null)
                photo.Secret = img.Secret;
            if (photo.Farm == null && img.Farm != null)
                photo.Farm = img.Farm;
            if (photo.Owner == null && img.Owner != null)
                photo.Owner = img.Owner;
            if (photo.Server == null && img.Server != null)
                photo.Server = img.Server;
            if (photo.Title == null && img.Title != null)
                photo.Title = img.Title;
        }

        public Photo DeserializePhoto(string json)
        {
            json = json.Replace("\"", "");

            return new Photo
            { 
                ID = TraverseString(json, "id"),
                Secret = TraverseString(json, "secret"),
                Server = TraverseString(json, "server"),
                Farm = TraverseString(json, "farm"),
                Camera = TraverseString(json, "camera")
            };
        }

        private string TraverseString(string json, string search, string endchar = ",")
        {
            var carrot = 0;
            var regex = @"\W" + search + @"\W";
            var first = Regex.Match(json, regex).Index + search.Length;
            carrot = first + ": ".Length;
            var second = json.IndexOf(endchar, carrot);
            return json.Substring(carrot, second - carrot);
        }
    }
}
