﻿using Common.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BusinessProvider.Providers;
using Flickr.Helpers;

namespace Business.Helpers
{
    public interface IFlickrDownloadPhotoHelper
    {
        Task<IList<Photo>> DownloadPhotoInformation(string token, int privacyLevel);
        Task<IList<Photo>> AssignExifInformation(string token, List<Photo> photos);
        Task<Photo> AttachExif(string token, Photo photo);
        List<Photo> GetNewPhotosToPersist(List<Photo> photos);
        IList<Photo> AssignUserIdentity(IList<Photo> photos, string userIdentity);
    }

    public class FlickrDownloadPhotoHelper : IFlickrDownloadPhotoHelper
    {
        private ISecurityMD5Helper _securityMD5Helper;
        private IBusinessProvider[] _flickrBusinessProvider;
        private IFlickrJSONHelper _flickrJSONHelper;

        public FlickrDownloadPhotoHelper()
        {
            _securityMD5Helper = new SecurityMD5Helper();
            _flickrBusinessProvider = new[]{ new FlickrBusinessProvider()};
            _flickrJSONHelper = new FlickrJSONHelper();
        }

        public IList<Photo> AssignUserIdentity(IList<Photo> photos, string userIdentity)
        {
            foreach(var photo in photos.Where(t => t.WalaUserID == null).ToList())
                photo.WalaUserID = userIdentity;
            return photos;
        }

        public async Task<Photo> AttachExif(string token, Photo photo)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                try
                {
                    var response = client.GetAsync(string.Format("https://api.flickr.com/services/rest/?method=flickr.photos.getExif{0}{1}{2}{3}{4}{5}",
                                    string.Format("&api_key={0}", _api_key),
                                    string.Format("&auth_token={0}", token),
                                    "&format=json",
                                    "&nojsoncallback=1",
                                    string.Format("&photo_id={0}", photo.ID),
                                    string.Format("&api_sig={0}", GenerateSignatureForExif(photo.ID, token))
                                    )).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var exifs = new List<Exif>();
                        string json = await response.Content.ReadAsStringAsync();
                        if (!json.Equals("{\"stat\":\"fail\",\"code\":96,\"message\":\"Invalid signature\"}"))
                        {
                            JObject googleSearch = JObject.Parse(json);
                            // get JSON result objects into a list
                            var img = _flickrJSONHelper.DeserializePhoto(json);
                            _flickrJSONHelper.AssignMissingPropertiesToPhoto(img, photo);
                            IList<JToken> results = googleSearch["photo"]["exif"].Children().ToList();
                            foreach (JToken result in results)
                            {
                                var exif = _flickrJSONHelper.DeserializeExif(result.ToString());
                                if (exif.Tag.EndsWith("DateTimeOriginal"))
                                    photo.OriginalDate = _flickrJSONHelper.ConvertRawToDateTime(exif.Raw);
                                photo.Exif.Add(exif);
                            }
                        }
                        return photo;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    var message = ex.Message;
                    return null;
                }
            }
        }

        public List<Photo> GetNewPhotosToPersist(List<Photo> photos)
        {
            var persistor = _flickrBusinessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (persistor == null)
                throw new Exception("Didnt find any business providers for the Flickr Service");
            return persistor.FindPhotosToPersist(photos);
        }

        public async Task<IList<Photo>> AssignExifInformation(string token, List<Photo> photos)
        {
            var photosToPersist = GetNewPhotosToPersist(photos);

            foreach (var photo in photosToPersist)
            {
                await AttachExif( token, photo);
            }
            return photosToPersist;
        }

        public async Task<IList<Photo>> DownloadPhotoInformation(string token, int privacyLevel)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = new TimeSpan(hours: 0, minutes: 0, seconds: 25);
                try
                {
                    var photos = new List<Photo>();
                    int page = 1;
                    var continueDownloading = true;
                    
                    while(continueDownloading){
                        var methods = GenerateMethodCallForAPI(page, privacyLevel, token);
                        
                        var response = client.GetAsync(string.Format("{0}{1}{2}",
                            _flickrAPI,
                            string.Join("&", methods),
                            string.Format("&api_sig={0}", GenerateSignatureForUser(methods))
                            )).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string json = await response.Content.ReadAsStringAsync();
                            if (json == "{\"stat\":\"fail\",\"code\":98,\"message\":\"Invalid auth token\"}")
                                throw new Exception("Invalid token");
                            JObject googleSearch = JObject.Parse(json);
                            // get JSON result objects into a list
                            IList<JToken> results = googleSearch["photos"]["photo"].Children().ToList();
    
                            foreach (JToken result in results)
                            {
                                var searchResult = JsonConvert.DeserializeObject<Photo>(result.ToString());
                                photos.Add(searchResult);
                            }
                            
                            int ant_pages = (int)googleSearch["photos"]["pages"];
                            page++;
                            if(page > ant_pages)
                                continueDownloading = false;                              
                        }
                    }
                    return photos;
                }
                catch (Exception ex)
                {
                    var message = ex.Message;
                    return new List<Photo>{
                        new Photo{Title = ex.Message, Camera = ex.StackTrace}
                    };
                }
            }
        }
        
        private List<string> GenerateMethodCallForAPI(int page, int privacyLevel, string token)
        {
           var methods = new List<string>();
            methods.Add(string.Format("method={0}", _flickrMethod));
            methods.Add(string.Format("api_key={0}", _api_key));
            methods.Add(string.Format("user_id={0}", _userID));
            methods.Add(string.Format("format={0}", _resultFormat));
            methods.Add(string.Format("privacy_filter={0}", privacyLevel));
            methods.Add(string.Format("per_page={0}", _resultPrPage));
            methods.Add(string.Format("page={0}", page));
            methods.Add(string.Format("nojsoncallback={0}", _nojsoncallback));
            methods.Add(string.Format("auth_token={0}", token));
           return methods;
        }
        
        private string GenerateSignatureForUser(List<string> inputValues)
        {
            var list = inputValues;
            list.Sort();
            list.Insert(0, _sharedSecret);
            return _securityMD5Helper.CalculateMD5Hash(string.Join(string.Empty, list.Select(t => t.Replace("=", string.Empty))));
        }

        public string GenerateSignatureForExif(string photoID, string token)
        {
            var method = "method" + "flickr.photos.getExif";
            var photoId = "photo_id";

            string argument = _sharedSecret + "api_key" + _api_key + "auth_token" + token + "formatjson" + method + "nojsoncallback1" + photoId + photoID;
            return _securityMD5Helper.CalculateMD5Hash(argument);
        }

        private string _flickrMethod = "flickr.people.getPhotos";
        private string _api_key = "ea07ff3024ae4aa421dd06de8c7b26a5";
        private string _userID = "30200242@N05";
        private string _resultFormat = "json";
        private string _resultPrPage = "500";
        private string _sharedSecret = "07ac74a456457725";
        private string _nojsoncallback = "1";
        private string _flickrAPI = "https://api.flickr.com/services/rest/?";
    }
}
