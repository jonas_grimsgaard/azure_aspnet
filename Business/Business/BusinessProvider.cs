﻿using Common.Model;
using Datalayer.Transactions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace BusinessProvider.Providers
{
    public interface IBusinessProvider
    {
        bool Accept(string accept);
        List<Photo> Persist(IList<Photo> photos);
        IList<Photo> Photos(string userID);
        IQueryable<Photo> PhotosQuery(string userId);
        List<Photo> FindPhotosToPersist(IList<Photo> photos);
        List<Photo> UpdateRecords(IList<Photo> photos);
        List<Photo> DeleteRecords(string[] photoIds);
    }

    [Export(typeof(IBusinessProvider))]
    public class FlickrBusinessProvider : IBusinessProvider
    {
        private PersistData _dataPersistor;
        public FlickrBusinessProvider()
        {
            _dataPersistor = new PersistData();
        }

        public bool Accept(string accept)
        {
            return accept == "Flickr";
        }

        public List<Photo> Persist(IList<Photo> photoList)
        {
            var photos = photoList.Select(t => (Photo)t).ToList();
            return _dataPersistor.PersistPhotos(photos);
        }

        public IList<Photo> Photos(string userID)
        {
            return null;
        }
        public IQueryable<Photo> PhotosQuery(string userId)
        {
            throw new NotImplementedException();
        }

        public List<Photo> FindPhotosToPersist(IList<Photo> photos)
        {
            //var photos = photoList.Select(t => (Photo)t).ToList();
            return _dataPersistor.FindPhotosNotExistsInDatabase(photos);
        }

        public List<Photo> UpdateRecords(IList<Photo> photos)
        {
            return _dataPersistor.UpdatePhotoInDB(photos);
        }

        public List<Photo> DeleteRecords(string[] photoIds)
        {
            return _dataPersistor.DeleteRecords(photoIds);
        }
    }

    
}
