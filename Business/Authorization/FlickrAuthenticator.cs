﻿using Business.Helpers;
using Business.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Business.Authorization
{
    public interface IFlickrAuthenticator 
    {
        string RequestToken(string frob, string method);
        string RequestLoginUrl(string secret);
    }

    public class FlickrAuthenticator : IFlickrAuthenticator
    {
        private string _flickrAPI = "https://api.flickr.com/services/rest/?";
        private string _api_key = "ea07ff3024ae4aa421dd06de8c7b26a5";
        private ISecurityMD5Helper _securityMD5Helper;

        public FlickrAuthenticator()
        {
            _securityMD5Helper = new SecurityMD5Helper();
        }

        public string RequestLoginUrl(string secret)
        {
            var attributeAccess = "read";

            var md5String = string.Format("{0}api_key{1}perms{2}", secret, _api_key, attributeAccess);

            var api_sig = _securityMD5Helper.CalculateMD5Hash(md5String);

            return string.Format("http://flickr.com/services/auth/?api_key={0}&perms={1}&api_sig={2}", _api_key, attributeAccess, api_sig);
        }

        public string RequestToken(string frob, string method)
        {
            var api_sign = GenerateSignature(frob, method);
            return RequestToken(_api_key, api_sign, method, frob, 1).Result;
        }

        private async Task<string> RequestToken(string api_key, string api_signed, string method, string frob, int callback)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var url = string.Format("{0}{1}{2}{3}{4}{5}{6}",
                    _flickrAPI,
                    string.Format("method={0}", method),
                    string.Format("&api_key={0}", api_key),
                    string.Format("&frob={0}", frob),
                    string.Format("&api_sig={0}", api_signed),
                    string.Format("&format=json"),
                    string.Format("&nojsoncallback={0}", callback)
                    );
                var response = client.GetAsync(url).Result;

                if (response.IsSuccessStatusCode)
                {
                    string res = await response.Content.ReadAsStringAsync();
                    if (res == "{\"stat\":\"fail\",\"code\":108,\"message\":\"Invalid frob\"}")
                        return "Invalid Frob!";
                    return DeserializeToToken(res).Token;
                }
                return response.StatusCode.ToString();
            }
        }

        private string GenerateSignature(string frob, string method, string secret = "07ac74a456457725")
        {
            string tomd5 = secret + "api_key" + _api_key + "formatjson" + "frob" + frob + "method" + method + "nojsoncallback1";
            return _securityMD5Helper.CalculateMD5Hash(tomd5);
        }

        private FlickrToken DeserializeToToken(string str)
        {
            var token = new FlickrToken();

            token.Token = SearchAndFind(str, "token\":{\"_content\":\"", "\"},\"perms");
            token.Perms = SearchAndFind(str, "perms\":{\"_content\":\"", "\"},\"user\"");
            token.User = new FlickrUser
            {
                Nsid = SearchAndFind(str, "nsid\":\"", "\",\"username"),
                UserName = SearchAndFind(str, "username\":\"", "\",\"fullname"),
                FullName = SearchAndFind(str, "fullname\":\"", "\"}},")
            };

            return token;
        }

        private static string SearchAndFind(string str, string firstValue, string secondValue)
        {
            int first = str.IndexOf(firstValue) + firstValue.Length;
            int last = str.LastIndexOf(secondValue);
            return str.Substring(first, last - first);
        }
    }
}
