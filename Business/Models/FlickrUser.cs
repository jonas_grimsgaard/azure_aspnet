﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class FlickrUser
    {
        public string Nsid { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}
