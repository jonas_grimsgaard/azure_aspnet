﻿namespace Business.Models
{
    public class FlickrToken
    {
        //{"auth":{"token":{"_content":"72157649495819403-c0b8f3d7f9386c90"},"perms":{"_content":"read"},"user":{"nsid":"30200242@N05","username":"jonasbg","fullname":"Jonas Grimsgaard"}},"stat":"ok"}
        public string Token { get; set; }
        public string Perms { get; set; }
        public FlickrUser User { get; set; }
        public string Stat { get; set; }
    }
}
