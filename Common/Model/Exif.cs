﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Common.Model
{
    //public interface IExif
    //{
    //    string Tag { get; set; }
    //    string Raw { get; set; }
    //    string Tagspace { get; set; }
    //    string TagspaceID { get; set; }
    //    string Label { get; set; }
    //    int DatabaseID { get; set; }
    //}


    public class Exif
    {
        public int DatabaseID { get; set; }

        public string Tagspace { get; set; }
        public string TagspaceID { get; set; }
        public string Tag { get; set; }
        public string Label { get; set; }
        public string Raw { get; set; }
        public virtual Photo PhotoID { get; set; }
    }
}
