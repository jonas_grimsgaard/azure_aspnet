﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Model
{
    //public interface IPhoto {
    //    string Farm { get; set; }
    //    string ID { get; set; }
    //    string Secret { get; set; }
    //    string Server { get; set; }
    //    string Camera { get; set; } 
    //    string Owner { get; set; } 
    //    string Title { get; set; }
    //    IList<IExif> Exif { get; set; }
    //    DateTime? OriginalDate { get; set; }
    //    string WalaUserID { get; set; }
    //}

    public class Photo
    {
        private List<Exif> _exif;

        public string Farm { get; set; }
        public string ID { get; set; }
        public string Secret { get; set; }
        public string Server { get; set; }
        public bool IsPublic { get; set; }
        public bool IsFriend { get; set; }
        public bool IsFamily { get; set; }

        public int DatabaseID { get; set; }
        public string Title { get; set; }
        public string Owner { get; set; }
        public string Camera { get; set; }
        public DateTime? OriginalDate { get; set; }
        public string WalaUserID { get; set; }

        public virtual List<Exif> Exif
        {
            get
            {
                if (_exif == null)
                    _exif = new List<Exif>();
                return _exif;
            }
            set
            {
                _exif = value;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Photo))
                return false;
            var photo = (Photo)obj;
            return (
                    (photo.Farm == this.Farm) &&
                    (photo.ID == this.ID) &&
                    (photo.IsFamily == this.IsFamily) &&
                    (photo.IsFriend == this.IsFriend) &&
                    (photo.IsPublic == this.IsPublic) &&
                    (photo.Owner == this.Owner) &&
                    (photo.Secret == this.Secret) &&
                    (photo.Server == this.Server) &&
                    (photo.Title == this.Title)
                    );
        }
    }
}
