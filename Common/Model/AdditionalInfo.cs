﻿using System.Collections.Generic;

namespace Common.Model
{
    public class AdditionalInfo
    {
        public int DatabaseID { get; set; }

        public List<Fave> Fave { get; set; }
        public List<Comment> Comments { get; set; }
    }

    public class Fave
    {
        public int DatabaseID { get; set; }
        public int AdditionalInfoID { get; set; }
        public string nsid { get; set; }
        public string UserName { get; set; }
        public string FavDate { get; set; }

    }

    public class Comment
    {
        public int DatabaseID { get; set; }
        public int AdditionalInfoID { get; set; }

        public string ID { get; set; }
        public string Author { get; set; }
        public string Authorname { get; set; }
        public string Datecreate { get; set; }
        public string Permalink { get; set; }
        public string Text { get; set; }

    }
}