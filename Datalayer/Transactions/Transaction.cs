﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datalayer.EntityFramework;

namespace Datalayer.Transactions
{
    public interface ITransaction
    {
    }

    [Export(typeof(ITransaction))]
    public class Transaction : ITransaction
    {
        public Photo GetPhoto(int id)
        {
            using (var context = new DataContext())
            {
                return context.Photos.FirstOrDefault(t => t.DatabaseID == id);
            }
        }

        public Photo GetPhoto(string identifier)
        {
            using (var context = new DataContext())
            {
                return context.Photos.FirstOrDefault(t => t.ID == identifier);
            }
        }

        public List<Photo> GetPhotoCollection(int[] ids)
        {
            using (var context = new DataContext())
            {
                return context.Photos.Where(t => ids.Contains(t.DatabaseID)).ToList();
            }
        }

        public IList<Photo> GetPhotosForUser(string userID)
        {
            using (var context = new DataContext())
            {
                return context.Photos.Where(t => t.Owner == userID).ToList();
            }
        }
        //http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg

        public IQueryable<Photo> GetPhotosForUserQuery(string userId)
        {
            using (var context = new DataContext())
            {
                return context.Photos.Where(t => t.Owner == userId);
            }
        }
    }
}
