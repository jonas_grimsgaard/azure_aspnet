﻿using Datalayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datalayer.EntityFramework;
using Common.Model;
using System.ComponentModel.Composition;
using System.Data.Entity;

namespace Datalayer.Transactions
{
    public interface IPersistData {
        List<Photo> PersistPhotos(List<Photo> photos);
        List<Photo> FindPhotosNotExistsInDatabase(IList<Photo> photos);
        List<Photo> UpdatePhotoInDB(IList<Photo> photos);
        List<Photo> DeleteRecords(string[] photoIds);
    }

    [Export(typeof(IPersistData))]
    public class PersistData : IPersistData
    {
        public List<Photo> PersistPhotos(List<Photo> photos)
        {
            using (var context = new DataContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                context.Photos.AddRange(photos);
                foreach (var photo in photos)
                {
                    var exif = photo.Exif.Select(t => (Exif)t).ToList();
                    context.Exifs.AddRange(exif);
                }
                context.SaveChanges();
                return photos;
            }
        }

        public List<Photo> FindPhotosNotExistsInDatabase(IList<Photo> photos)
        {
            using (var context = new DataContext())
            {
                var photosReturning = new List<Photo>();
                foreach (var photo in photos)
                    if (!context.Photos.Any(n => n.ID.Contains(photo.ID)))
                        photosReturning.Add(photo);
                return photosReturning;
            }
        }

        public List<Photo> UpdatePhotoInDB(IList<Photo> photos)
        {
            using (var context = new DataContext())
            {
                context.Configuration.ProxyCreationEnabled = false;
                var updatedPhotos = new List<Photo>();
                foreach (var photo in photos)
                {
                    var dbphoto = context.Photos.FirstOrDefault(t => t.ID == photo.ID);
                    if (dbphoto != null)
                    {
                        if (!dbphoto.Equals(photo))
                        {
                            UpdateEntityFromProvider(photo, dbphoto);
                            updatedPhotos.Add(dbphoto);
                        }
                    }
                }
                context.SaveChanges();
                return updatedPhotos;
            }
        }

        public List<Photo> DeleteRecords(string[] photoIds)
        {
            using (var context = new DataContext())
            {
                var deletedList = new List<Photo>();
                //var photosNotOnFlickr = context.Photos.Where(t => !photoIds.Any(n => n == t.ID)).ToList();
                var ids = context.Photos.Select(n => n.ID).ToList();
                var idsToDelete = ids.Except(photoIds).ToList();
                var photosNotOnFlickr = context.Photos.Where(t => idsToDelete.Contains(t.ID)).ToList();
                if (photosNotOnFlickr != null && photosNotOnFlickr.Count > 0)
                {
                    var exifs = context.Exifs.Where(t => idsToDelete.Contains(t.PhotoID.ID)).ToList();
                    deletedList.AddRange(photosNotOnFlickr);
                    context.Photos.RemoveRange(photosNotOnFlickr);
                    context.Exifs.RemoveRange(exifs);
                    context.SaveChanges();
                }
                return deletedList;
            }
        }

        private static void UpdateEntityFromProvider(Photo photo, Photo dbphoto)
        {
            dbphoto.IsFamily = photo.IsFamily;
            dbphoto.IsFriend = photo.IsFriend;
            dbphoto.IsPublic = photo.IsPublic;
            dbphoto.Owner = photo.Owner;
            dbphoto.Secret = photo.Secret;
            dbphoto.Server = photo.Server;
            dbphoto.Title = photo.Title;
            if(photo.Exif.Count > 0)
                dbphoto.Exif = photo.Exif;
            if(photo.OriginalDate != null)
                dbphoto.OriginalDate = photo.OriginalDate;
            if(photo.Camera != null)
                dbphoto.Camera = photo.Camera;
        }

        
    }
}
