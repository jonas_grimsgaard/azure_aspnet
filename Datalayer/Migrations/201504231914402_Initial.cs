namespace Datalayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exifs",
                c => new
                    {
                        DatabaseID = c.Int(nullable: false, identity: true),
                        Tagspace = c.String(),
                        TagspaceID = c.String(),
                        Tag = c.String(),
                        Label = c.String(),
                        Raw = c.String(),
                        PhotoID_DatabaseID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DatabaseID)
                .ForeignKey("dbo.Photos", t => t.PhotoID_DatabaseID, cascadeDelete: true)
                .Index(t => t.PhotoID_DatabaseID);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        DatabaseID = c.Int(nullable: false, identity: true),
                        Farm = c.String(),
                        ID = c.String(),
                        Secret = c.String(),
                        Server = c.String(),
                        IsPublic = c.Boolean(nullable: false),
                        IsFriend = c.Boolean(nullable: false),
                        IsFamily = c.Boolean(nullable: false),
                        Title = c.String(),
                        Owner = c.String(),
                        Camera = c.String(),
                        OriginalDate = c.DateTime(),
                        WalaUserID = c.String(),
                    })
                .PrimaryKey(t => t.DatabaseID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Exifs", "PhotoID_DatabaseID", "dbo.Photos");
            DropIndex("dbo.Exifs", new[] { "PhotoID_DatabaseID" });
            DropTable("dbo.Photos");
            DropTable("dbo.Exifs");
        }
    }
}
