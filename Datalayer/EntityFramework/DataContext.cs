﻿using Common.Model;
using Datalayer.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datalayer.EntityFramework
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("PhotoContext")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Migrations.Configuration>());
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataContext>());
        }

        public DbSet<Photo> Photos { get; set; }
        public DbSet<Exif> Exifs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Photo>().HasKey(t => t.DatabaseID);
            modelBuilder.Entity<Photo>()
                .Property(t => t.DatabaseID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Exif>()
                .HasKey(t => t.DatabaseID)
                .Property(x => x.DatabaseID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Exif>()
                .HasRequired<Photo>(t => t.PhotoID)
                .WithMany(m => m.Exif)
                .WillCascadeOnDelete();
        }
    }
}
