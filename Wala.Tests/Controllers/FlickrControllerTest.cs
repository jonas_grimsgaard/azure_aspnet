﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wala.Tests.Controllers
{
    [TestFixture]
    public class FlickrControllerTest
    {
        [Test]
        public void JSonParserShouldParseFlickrAPIData()
        {
            var response = FlickrAPIReturnValue();
            Assert.That(response, Is.Not.Null);
        }

        private string FlickrAPIReturnValue()
        {
            var path = "JSONresponse.txt";
            return File.ReadAllText(path); 
        }
    }
}
