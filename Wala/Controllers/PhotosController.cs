﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using Common.Model;
using Datalayer.EntityFramework;
using System.Web.Http.OData.Query;
using System.Security.Principal;
using System.Threading;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Wala.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using Common.Model;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Photo>("Photos");
    builder.EntitySet<Exif>("Exifs"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    [Authorize]
    public class PhotosController : ODataController
    {
        private DataContext db = new DataContext();

        // GET: odata/Photos
        [EnableQuery(PageSize = 10, AllowedQueryOptions = AllowedQueryOptions.Skip | AllowedQueryOptions.Select | AllowedQueryOptions.Filter | AllowedQueryOptions.OrderBy)]
        public IQueryable<Photo> GetPhotos()
        {
            var userID = HttpContext.Current.User.Identity.GetUserId();

            return db.Photos.Where(t => t.WalaUserID.Contains(userID));
        }

        // GET: odata/Photos(5)
        [EnableQuery]
        public SingleResult<Photo> GetPhoto([FromODataUri] int key)
        {
            return SingleResult.Create(db.Photos.Where(photo => photo.DatabaseID == key));
        }

        // PUT: odata/Photos(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<Photo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Photo photo = db.Photos.Find(key);
            if (photo == null)
            {
                return NotFound();
            }

            patch.Put(photo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhotoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(photo);
        }

        // POST: odata/Photos
        public IHttpActionResult Post(Photo photo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Photos.Add(photo);
            db.SaveChanges();

            return Created(photo);
        }

        // PATCH: odata/Photos(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Photo> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Photo photo = db.Photos.Find(key);
            if (photo == null)
            {
                return NotFound();
            }

            patch.Patch(photo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhotoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(photo);
        }

        // DELETE: odata/Photos(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            Photo photo = db.Photos.Find(key);
            if (photo == null)
            {
                return NotFound();
            }

            db.Photos.Remove(photo);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Photos(5)/Exif
        [EnableQuery]
        public IQueryable<Exif> GetExif([FromODataUri] int key)
        {
            return db.Photos.Where(m => m.DatabaseID == key).SelectMany(m => m.Exif);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PhotoExists(int key)
        {
            return db.Photos.Count(e => e.DatabaseID == key) > 0;
        }
    }
}
