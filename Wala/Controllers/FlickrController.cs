﻿using System.Linq;
using System.Web.Mvc;
using BusinessProvider.Providers;
using Microsoft.AspNet.Identity;
using Business.Authorization;
using Business.Helpers;
using Wala.Models;
using Common.Model;
using System.Collections.Generic;
using System;
using Flickr.Helpers;

namespace Wala.Controllers
{
    [Authorize]
    public class FlickrController : Controller
    {
        private string _secret = "07ac74a456457725";
        private IFlickrAuthenticator _flickrAuthenticator;
        private IFlickrDownloadPhotoHelper _flickrDownloadPhotoHelper;
        private ISecurityMD5Helper _securityMD5Helper;
        private IBusinessProvider[] _businessProvider;
        private IFlickrJSONHelper _flickrJSONHelper;

        public FlickrController()
        {
            _flickrAuthenticator = new FlickrAuthenticator();
            _flickrDownloadPhotoHelper = new FlickrDownloadPhotoHelper();
            _securityMD5Helper = new SecurityMD5Helper();
            _businessProvider = new[] { new FlickrBusinessProvider() };
            _flickrJSONHelper = new FlickrJSONHelper();
        }

        public ActionResult Index()
        {
            var model = RetrieveSession();
            model.LoginUrl = _flickrAuthenticator.RequestLoginUrl(_secret);
            if (model.Token == null || model.Token == "Invalid Frob!") 
            {              
                model.Frob = Request.Params["frob"];
                if(model.Frob != null)
                    model.Token = RequestToken(model);
            }
         
            if (model.Token != null)
            {
                StoreSession(model); 
            }
            return View(model);
            //return "<a href='" + loginUrl + "'>Login</a>";
        }
        
        private FlickrModel RetrieveSession(){
            //(FlickrModel)Session["object"];
            var model = new FlickrModel();
            if(Request.Cookies["sessionState"] != null){
                model.Token = Server.HtmlEncode(Request.Cookies["sessionState"]["token"]);
                model.LoginUrl = Server.HtmlEncode(Request.Cookies["sessionState"]["url"]);
            }
            return model;
        }
        
        private void StoreSession(FlickrModel model){
            Response.Cookies["sessionState"]["token"] = model.Token;
            Response.Cookies["sessionState"]["url"] = model.LoginUrl;
            Response.Cookies["sessionState"].Expires = DateTime.Now.AddDays(3);
        }

        [HttpPost]
        public ActionResult Persist(string token, Photo photo)
        {
            var flickrPersistor = _businessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (flickrPersistor == null)
                throw new Exception("No persistor found for Flickr service");
            _flickrDownloadPhotoHelper.AssignUserIdentity(new List<Photo>() { photo }, User.Identity.GetUserId());
            photo = _flickrDownloadPhotoHelper.AttachExif(token, photo).Result;
            return Content(_flickrJSONHelper.GenerateReturnJSON(flickrPersistor.Persist(new List<Photo>() { photo })), "application/json");
        }

        //1
        [HttpGet]
        public ActionResult DownloadAllFlickrPhotos(string token, int privacy)
        {
            return Content(_flickrJSONHelper.GenerateReturnJSON(_flickrDownloadPhotoHelper.DownloadPhotoInformation(token, privacy).Result.ToList()), "application/json");      
        }

        //2
        [HttpPost]
        public ActionResult FindPhotosNotInDatabase(List<Photo> photos)
        {
            return Content(_flickrJSONHelper.GenerateReturnJSON(_flickrDownloadPhotoHelper.GetNewPhotosToPersist(photos)), "application/json");
        }

        //3 til svaret fra 2
        [HttpPost]
        public ActionResult AttachExifToPhoto(string token, Photo photo)
        {
            return Content(_flickrJSONHelper.GenerateReturnJSON(_flickrDownloadPhotoHelper.AttachExif(token, photo).Result), "application/json");
        }

        [HttpPost]
        public ActionResult PersistPhotos(List<Photo> photos)
        {
            var flickrPersistor = _businessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (flickrPersistor == null)
                throw new Exception("No persistor found for Flickr service");
            _flickrDownloadPhotoHelper.AssignUserIdentity(photos, User.Identity.GetUserId());
            return Content(_flickrJSONHelper.GenerateReturnJSON(flickrPersistor.Persist(photos)), "application/json");
        }

        [HttpPost]
        public ActionResult PersistPhoto(string token, Photo photo)
        {
            var flickrPersistor = _businessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (flickrPersistor == null)
                throw new Exception("No persistor found for Flickr service");
            _flickrDownloadPhotoHelper.AssignUserIdentity(new List<Photo>(){photo}, User.Identity.GetUserId());
            Photo persist = _flickrDownloadPhotoHelper.AttachExif(token, photo).Result;
            List<Photo> persisted = flickrPersistor.Persist(new List<Photo>()
            { persist });

            return Content(_flickrJSONHelper.GenerateReturnJSON(persisted), "application/json");
        }

        [HttpPost]
        public ActionResult UpdateRecords(List<Photo> photos)
        {
            var flickrPersistor = _businessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (flickrPersistor == null)
                throw new Exception("No persistor found for Flickr service");
            _flickrDownloadPhotoHelper.AssignUserIdentity(photos, User.Identity.GetUserId());
            return Content(_flickrJSONHelper.GenerateReturnJSON(flickrPersistor.UpdateRecords(photos)), "application/json");
        }

        [HttpPost]
        public ActionResult UpdateRecord(Photo photo)
        {
            var flickrPersistor = _businessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (flickrPersistor == null)
                throw new Exception("No persistor found for Flickr service");
            _flickrDownloadPhotoHelper.AssignUserIdentity(new List<Photo>() { photo }, User.Identity.GetUserId());
            return Content(_flickrJSONHelper.GenerateReturnJSON(flickrPersistor.UpdateRecords(new List<Photo>() { photo })), "application/json");
        }

        [HttpGet]
        public ActionResult GetUserIdentity()
        {
            if (User.Identity.IsAuthenticated)
                return Json(User.Identity.GetUserId(), JsonRequestBehavior.AllowGet);
            return Json("User is not authenticated", JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public ActionResult DeletePhotos(List<Photo> photos)
        {
            var flickrPersistor = _businessProvider.FirstOrDefault(t => t.Accept("Flickr"));
            if (flickrPersistor == null)
                throw new Exception("No persistor found for Flickr service");

            return Content(_flickrJSONHelper.GenerateReturnJSON(
                flickrPersistor.DeleteRecords(photos.Select(t => t.ID).ToArray()))
                , "application/json");
        }

        private string RequestToken(FlickrModel model)
        {
           string method = "flickr.auth.getToken";
           return _flickrAuthenticator.RequestToken(model.Frob, method);
        }

        public ActionResult Photo()
        {
            return View();
        }

        public ActionResult Moments()
        {
            return View();
        }
    }
}
