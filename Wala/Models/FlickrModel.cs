﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Model;

namespace Wala.Models
{
    public class FlickrModel
    {
        private IList<Photo> _updatedPhotos { get; set; }
        private IList<Photo> _persistedPhotos { get; set; }

        public IList<Photo> UpdatedPhotos 
        { 
            get 
            {
                if (_updatedPhotos == null)
                    _updatedPhotos = new List<Photo>();
                return _updatedPhotos;
            }
            set { _updatedPhotos = value; }
        }

        public IList<Photo> PersistedPhotos
        {
            get
            {
                if (_persistedPhotos == null)
                    _persistedPhotos = new List<Photo>(); 
                return _persistedPhotos;
            }
            set { _persistedPhotos = value; }
        }
        public string LoginUrl { get; set; }
        public string Frob { get; set; }
        public string Token { get; set; }
    }
}