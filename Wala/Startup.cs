﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Wala.Startup))]
namespace Wala
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
